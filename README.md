# YouTube videos or playlists to mp3

My first ever script!
It downloads videos and playlists and converts them to mp3.

To run the script curl must be installed.

After you download the script:

make it executable:

`chmod +x video2mp3`

then move video2mp3 to `/usr/local/bin`: 

`sudo mv video2mp3 /usr/local/bin`

Also works with Dailymotion & Vimeo links.

Now you can use this script like any other terminal program.

Feel free  to download use or modify to your liking.